
//Funções de Sucesso e Erro
 function onSuccess(heading) {
   var magneticHeading = heading.magneticHeading;

   //Como o número vem com muitas casas decimais, resolvi mostrar só as 2 primeiras
   magneticHeading.toFixed(2);

   document.getElementById("compass-status").innerHTML = 'Heading: ' + magneticHeading + 'º';
 };

 function onError(error) {
   document.getElementById("compass-status").innerHTML = 'CompassError: ' + error.code;
 };


var app = {

    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    onDeviceReady: function() {
        this.receivedEvent('deviceready');
        document.getElementById("compass-status").innerHTML = 'Device ready';



        //Opções do compass (no caso coloquei para disparar a cada 1/4 segundo)
        var options = {
            frequency: 250
        };


        //Quando disparar o evento "DeviceReady", chama o compass e as funções de sucesso e erro
        //A documentação está nesse link aqui: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-device-orientation/
        var watchID = navigator.compass.watchHeading(onSuccess, onError, options);
    },


    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

//Inicializa o app
app.initialize();
